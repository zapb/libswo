Hacking
=======

This document describes how to start hacking on the libswo project.
Make sure you read through the README file before continuing.


Coding style
------------

This project uses the Linux kernel coding style where appropiate, see
<https://www.kernel.org/doc/html/latest/process/coding-style.html> for details.

Amendments to the Linux kernel coding style:

 - Do not use goto statements.
 - Always declare variables at the beginng of a function.
 - Do not assign values to variables at declaration time.


Contributions
-------------

The following ways can be used to submit a contribution to the libswo project:

 - Send patches generated with `git format-patch`.
 - Push your changes to a public Git repository and send the URL where to pull
   them from.

In any case, send directly to <dev@zapb.de>.
Before submitting, please consider the following:

 - Every single patch must be compilable.
 - Develop your contribution against the current Git master branch.
 - Check your contribution for memory leaks and similar errors by using
   *valgrind*.


Bug reports
-----------

Send bug reports directly to <dev@zapb.de>.
Please try to include all of the following information in your report:

 - Instructions to reproduce the bug (e.g., command-line invocations)
 - Trace data which causes the problem
 - Debug log output of libswo
 - Information about your environment:
   - Version of libswo
   - Operating system (e.g., Debian GNU/Linux 8.4)
 - Backtraces if the application using libswo is crashing.

If the bug report is for a regression, additionally include the information
above about the working version where appropiate.

Please develop and attach a patch that solves the reported bug, if possible.
See the guidelines for contributions above.


Random notes
------------

 - Always use `log_err()`, `log_warn()`, `log_info()` and `log_dbg()` to output
   messages. Never use `printf()` or similar functions directly.
