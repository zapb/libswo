libswo
======

libswo is a shared library written in C to decode SWO trace data.


Requirements
------------

libswo depends on the following packages:

 - GCC (>= 4.0) or Clang
 - Make
 - pkg-config >= 0.23
 - Doxygen (optional, only required for API documentation)

If you're building libswo from Git, the following packets are additionally
required:

 - Git
 - Libtool
 - Autoconf >= 2.64
 - Automake >= 1.9

Requirements for the C++ bindings:

 - libswo >= 0.1.0
 - C++ compiler with C++11 support, e.g.
   - GCC >= 4.7
   - Clang >= 3.0

Requirements for the Python bindings:

 - libswocxx >= 0.1.0 (libswo C++ bindings, see above)
 - Python >= 3.2
 - Python setuptools
 - SWIG >= 3.0.3


Building and installing
-----------------------

In order to get and build the latest Git version of libswo, run the following
commands:

    $ git clone https://gitlab.zapb.de/zapb/libswo.git
    $ cd libswo
    $ ./autogen.sh
    $ ./configure
    $ make

After `make` finishes without any errors, use the following command to install
libswo:

    $ make install


Copyright and license
---------------------

libswo is licensed under the terms of the GNU General Public License (GPL),
version 3 or later. See COPYING file for details.


Website
-------

<https://gitlab.zapb.de/zapb/libswo.git>
