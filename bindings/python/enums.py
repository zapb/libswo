##
## This file is part of the libswo project.
##
## Copyright (C) 2017 Marc Schink <swo-dev@marcschink.de>
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.
##

from enum import IntEnum, IntFlag

class LogLevel(IntEnum):
    NONE = 0
    ERROR = 1
    WARNING = 2
    INFO = 3
    DEBUG = 4

class PacketType(IntEnum):
    UNKNOWN = 0
    SYNC = 1
    OF = 2
    LTS = 3
    GTS1 = 4
    GTS2 = 5
    EXT = 6
    INST = 7
    HW = 8
    DWT_EVTCNT = 16
    DWT_EXCTRC = 17
    DWT_PC_SAMPLE = 18
    DWT_PC_VALUE = 19
    DWT_ADDR_OFFSET = 20
    DWT_DATA_VALUE = 21

class LocalTimestampRelation(IntEnum):
    SYNC = 0
    TS = 1
    SRC = 2
    BOTH = 3

class ExtensionSource(IntEnum):
    ITM = 0
    HW = 1

class ExceptionTraceFunction(IntEnum):
    RESERVED = 0
    ENTER = 1
    EXIT = 2
    RETURN = 3

class DecoderFlags(IntFlag):
    NONE = 0
    EOS = (1 << 0)
